mutation registeruser{
  registerUser(email: "surya@gmail.com",username: "surya", password1: "vijayvijay", password2: "vijayvijay"){
    success
  }
}

mutation token{
  tokenAuth(email: "surya@gmail.com", password: "vijayvijay"){
    token
  }
}

mutation createcourse{
  createCourse(authorDesignation: "teacher", whatYouWillLearn:"everything", title: "python", price: 123, stats: "new version", language: "E", description: "a to z", requirements: "basic of pythons"){
    author
    title
  }
}

mutation createmodule{
  createModule(courseId: 1, name: "django", prerequisite: "basics of django"){
    name
  }
}

mutation createlesson{
  createLesson(name: "django basics", description: "learn everything", transcript: "by video", prerequisite: "basics of django", moduleId:1){
    lesson
    module{
      name
    }
  }
}

mutation courseenroll{
  courseEnroll(courseId:1){
    course{
      title
    }
    username{
      username
    }
  }
}

mutation rating{
  createRating(courseId:1, rating:5){
    user{
      username
    }course{
      title
    }
  }
}


mutation completedlesson{
  completedLesson(isCompleted: "True", lessonId:1){
    user{
      username
    }lesson{
      name
    }
  }
}

mutation completedmodule{
  completedModule(isCompleted: "True", moduleId:1){
    user{
      username
    }module{
      name
    }
  }
}

mutation completedcourse{
  completedCourse(courseId:1, isCompleted:"True"){
    user{
      username
    }course{
      title
    }
  }
}

mutation createquiz{
  createQuiz(lessonId:1, title: "Python quiz"){
    lesson{
      name
    }title
  }
}

mutation createquestion{
  createQuestion(quizId:1, question: "what is list", mark:5, questionType:"FB"){
    question
    quiz{
      title
    }
  }
}

mutation createanswers{
  createAnswer(questionId: 6, answer: "something", answerType: "FB"){
    message
  }
}

mutation postanswer{
  postAnswer(questionId: 6, answer: "something", answerType:"MC"){
    message
  }
}