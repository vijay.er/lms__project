import graphene
from graphene_django.debug import DjangoDebug
from graphene_file_upload.scalars import Upload

import lms_graphql.quiz_graphql.mutation as quiz_mutation
import lms_graphql.lms.mutation as lms_mutation

import lms_graphql.quiz_graphql.schema as quiz_schema
import lms_graphql.lms.schema as lms_schema
import lms_graphql.user_auth.schema as user_schema


class Query(lms_schema.Query,
            user_schema.Query,
            quiz_schema.Query):
    debug = graphene.Field(DjangoDebug, name="_debug")


class Mutation(lms_mutation.Mutation,
               user_schema.Mutation,
               quiz_mutation.Mutation,
               graphene.ObjectType):
    pass


schema = graphene.Schema(query=Query, mutation=Mutation, types=[Upload])
