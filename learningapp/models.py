from django.contrib.auth import get_user_model
# from django.contrib.auth.models import settings.AUTH_USER_MODEL
from django.db import models
from django.db.models import Avg, Sum
import datetime
from mutagen.mp4 import MP4
from django.db.models.signals import pre_save, post_save
from django.conf import settings
from django.contrib.auth.models import AbstractUser

# User._meta.get_field('email')._unique = True
# User = get_user_model()


class CustomUser(AbstractUser):
    is_author = models.BooleanField(default=False)

    def __str__(self):
        return self.username


class DateTimeModel(models.Model):
    """ A base model with created and edited datetime fields """

    created = models.DateTimeField(auto_now_add=True)
    edited = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Category(DateTimeModel):
    """category details for course"""
    category_name = models.CharField(max_length=50)

    def __str__(self):
        return self.category_name


class Course(DateTimeModel):
    """ course details """

    languages = (
        ('E', 'English'),
        ('H', 'Hindi')
    )

    title = models.CharField(max_length=100, unique=True)
    # slug = models.SlugField(max_length=100, null=False)
    featured_image = models.FileField(blank=True, upload_to='images')
    author = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='courses', on_delete=models.CASCADE)
    author_designation = models.CharField(max_length=50)
    stats = models.CharField(max_length=200)
    description = models.TextField(max_length=1000)
    language = models.CharField(max_length=100, choices=languages)
    rating = models.FloatField(blank=True, default=0.0)
    price = models.IntegerField()
    what_you_will_learn = models.TextField(max_length=1000)
    requirements = models.CharField(max_length=500)
    related_course = models.ManyToManyField('Course', related_name='courses', blank=True)
    category = models.ForeignKey(Category, related_name='courses', on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True, blank=True)

    # learners = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='learners_course', blank=True)

    def __str__(self):
        return self.title + '' + str(self.id)


class Rating(models.Model):
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='ratings')
    rating = models.IntegerField(blank=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.user)

    class Meta:
        verbose_name_plural = 'Rating'


def average_rating(sender, instance, **kwargs):
    """ for average rating based on course"""
    reviews = Rating.objects.filter(course=instance.course).aggregate(Avg("rating"))
    avg_rating = Course.objects.filter(title=instance.course).update(rating=(round(reviews['rating__avg'], 1)))


post_save.connect(average_rating, sender=Rating)


class Module(DateTimeModel):
    """ Module details """

    name = models.CharField(max_length=100)
    # slug = models.SlugField(max_length=100, null=False)
    duration = models.TimeField(blank=True, default=datetime.time(0, 0, 0))
    prerequisite = models.CharField(max_length=100)
    is_active = models.BooleanField(default=True, blank=True)
    course = models.ForeignKey(Course, related_name='modules', on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id) + ',' + self.name

    class Meta:
        verbose_name_plural = 'Module'


class Lesson(DateTimeModel):
    """ Lesson details """

    name = models.CharField(max_length=100)
    featured_image = models.FileField(upload_to='images')
    description = models.TextField(max_length=500)
    video = models.FileField(upload_to='videos')
    file = models.FileField(upload_to='files')
    duration = models.TimeField(max_length=20, blank=True)
    transcript = models.TextField(max_length=1000)
    prerequisite = models.CharField(max_length=100)
    is_active = models.BooleanField(default=True)
    module = models.ForeignKey(Module, related_name='lessons', on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id) + ',' + self.name

    class Meta:
        verbose_name_plural = 'Lesson'


def get_duration(sender, instance, **kwargs):
    """ for calculate the duration of the video in lesson model and
    updating the total duration of the Modules model"""

    video_time = datetime.timedelta(seconds=int(MP4(instance.video).info.length))
    module = Module.objects.values_list('duration', flat=True).filter(name=instance.module)
    for i in module:
        total_duration = datetime.timedelta(hours=i.hour, minutes=i.minute, seconds=i.second)
        module.update(duration=str(total_duration + video_time))
    instance.duration = str(video_time)


pre_save.connect(get_duration, sender=Lesson)


class CourseEnroll(DateTimeModel):
    """ course enroll details by user """
    username = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='course_enroll', on_delete=models.CASCADE)
    course = models.ForeignKey(Course, related_name='course_enroll', on_delete=models.CASCADE)
    registered_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.username)


class CompletedLessons(DateTimeModel):
    """ completed lesson details by user"""
    lesson = models.ForeignKey(Lesson, related_name='completed_lesson', on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='completed_lesson', on_delete=models.CASCADE)
    is_active = models.BooleanField()

    def __str__(self):
        return self.lesson.name


class CompletedModules(DateTimeModel):
    """ completed module details by user"""
    module = models.ForeignKey(Module, related_name='completed_module', on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='completed_module', on_delete=models.CASCADE)
    is_active = models.BooleanField()

    def __str__(self):
        return self.module.name


class CompletedCourses(DateTimeModel):
    """ completed course details by user"""
    course = models.ForeignKey(Course, related_name='completed_course', on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='completed_course', on_delete=models.CASCADE)
    is_active = models.BooleanField()

    def __str__(self):
        return self.course.title

