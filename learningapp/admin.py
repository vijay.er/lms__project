from django.contrib import admin
from learningapp.models import *
from quiz.models import *

admin.site.register(Category)

admin.site.register(Course)
admin.site.register(Module)
admin.site.register(Lesson)
admin.site.register(CustomUser)

admin.site.register(CourseEnroll)
admin.site.register(Rating)

admin.site.register(CompletedLessons)
admin.site.register(CompletedModules)
admin.site.register(CompletedCourses)
