from django.contrib.auth.models import User
from django.core.validators import validate_email


def get_user(value):
    try:
        user = User.objects.get(email=value.lower())
        return user.username
    except User.DoesNotExist:
        return None


def email_verify(value):
    try:
        validate_email(value)
        return True
    except:
        return None
