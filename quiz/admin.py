from django.contrib import admin
# from .forms import RegisterForm
from learningapp.models import *
from quiz.models import *


admin.site.register(QuizTakers)

admin.site.register(QuestionTF)
admin.site.register(QuestionFB)
admin.site.register(QuestionMC)
admin.site.register(QuestionET)
admin.site.register(CreateAnswer)
admin.site.register(Answer)


class QuizAdmin(admin.ModelAdmin):
    model = Quiz
    readonly_fields = ['no_of_questions', 'total_mark']


admin.site.register(Quiz, QuizAdmin)


class FillInBlankAdmin(admin.TabularInline):
    model = QuestionFB


class MultiChoiceAdmin(admin.TabularInline):
    model = QuestionMC


class TrueFalseAdmin(admin.TabularInline):
    model = QuestionTF


class EssayAdmin(admin.TabularInline):
    model = QuestionET


class QuestionAdmin(admin.ModelAdmin):
    inlines = [MultiChoiceAdmin, TrueFalseAdmin, FillInBlankAdmin, EssayAdmin]


admin.site.register(Question, QuestionAdmin)


# class EssayTypeAnswerAdmin(admin.TabularInline):
#     model = AnswerEssayType
#
#
# class MultiChoiceAnswerAdmin(admin.TabularInline):
#     model = AnswerMC
#
#
# class FillInBlankAnswerAdmin(admin.TabularInline):
#     model = AnswerFB
#
#
# class TrueFalseAnswerAdmin(admin.TabularInline):
#     model = AnswerTF


# class AnswerAdmin(admin.ModelAdmin):
#     inlines = [EssayTypeAnswerAdmin, MultiChoiceAnswerAdmin, FillInBlankAnswerAdmin, TrueFalseAnswerAdmin]



# admin.site.register(AnswerEssayType)
# admin.site.register(AnswerMC)
# admin.site.register(AnswerFB)
# admin.site.register(AnswerTF)
