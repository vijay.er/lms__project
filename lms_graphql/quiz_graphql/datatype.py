from graphene_django.types import DjangoObjectType
from graphene import Node
from quiz.models import *


class QuizNode(DjangoObjectType):
    class Meta:
        model = Quiz
        interfaces = (Node,)
        filter_fields = '__all__'


class QuestionNode(DjangoObjectType):
    class Meta:
        model = Question
        interfaces = (Node,)
        filter_fields = '__all__'


class MCQuestionNode(DjangoObjectType):
    class Meta:
        model = QuestionMC
        interfaces = (Node,)
        filter_fields = '__all__'


class TFQuestionNode(DjangoObjectType):
    class Meta:
        model = QuestionTF
        interfaces = (Node,)
        filter_fields = '__all__'


class FBQuestionNode(DjangoObjectType):
    class Meta:
        model = QuestionFB
        interfaces = (Node,)
        filter_fields = '__all__'


class ETQuestionNode(DjangoObjectType):
    class Meta:
        model = QuestionET
        interfaces = (Node,)
        filter_fields = '__all__'


class PostAnswerNode(DjangoObjectType):
    class Meta:
        model = Answer
        interfaces = (Node,)
        filter_fields = '__all__'


class AnswerNode(DjangoObjectType):
    class Meta:
        model = CreateAnswer
        interfaces = (Node,)
        filter_fields = '__all__'