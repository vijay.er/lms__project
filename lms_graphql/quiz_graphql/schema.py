from graphene_django.filter import DjangoFilterConnectionField
from lms_graphql.lms.resolvers import resolve_user
from lms_graphql.quiz_graphql.datatype import QuizNode, QuestionNode, AnswerNode
from quiz.models import Quiz, Question, Answer
import graphene


class Query(graphene.ObjectType):
    all_quiz = DjangoFilterConnectionField(QuizNode)
    quiz = graphene.List(QuizNode)

    def resolve_all_quiz(self, info):
        resolve_user(info)
        return Quiz.objects.all()

    def resolve_quiz(self, info):
        resolve_user(info)
        return Quiz.objects.all()

    all_question = DjangoFilterConnectionField(QuestionNode)
    question = graphene.List(QuestionNode)

    def resolve_all_question(self, info):
        resolve_user(info)
        return Question.objects.all()

    def resolve_question(self, info):
        resolve_user(info)
        return Question.objects.all()

    all_answer = DjangoFilterConnectionField(AnswerNode)
    answer = graphene.List(AnswerNode)

    def resolve_all_answer(self, info):
        resolve_user(info)
        return Answer.objects.all()

    def resolve_answer(self, info):
        resolve_user(info)
        return Answer.objects.all()
