import graphene
from graphql import GraphQLError
from lms_graphql.lms.datatypes import LessonNode
from lms_graphql.lms.resolvers import resolve_permission
from lms_graphql.user_auth.datatypes import UserTypeNode
from lms_graphql.quiz_graphql.datatype import *


class CreateQuiz(graphene.Mutation):
    # arguments for CreateLesson mutation
    class Arguments:
        lesson_id = graphene.Int(required=True)
        title = graphene.String(required=True)

    # return response for the enroll_course mutation
    title = graphene.String()
    lesson = graphene.Field(LessonNode)

    # mutation for the current lesson
    def mutate(self, info, lesson_id, title):
        lesson_name = Lesson.objects.get(id=lesson_id)

        user = info.context.user
        resolve_permission(user)
        if not user.is_author:
            raise GraphQLError("you are not the author")
        if not lesson_name.module.course.author == user:
            raise GraphQLError("you're not author of this course")

        quiz = Quiz(title=title,
                    lesson=lesson_name)

        quiz.save()
        return CreateQuiz(title=quiz.title,
                          lesson=quiz.lesson)


class CreateQuestion(graphene.Mutation):
    # arguments for CreateLesson mutation
    class Arguments:
        quiz_id = graphene.Int(required=True)
        question = graphene.String(required=True)
        mark = graphene.Int(required=True)
        question_type = graphene.String(required=True)
        option1 = graphene.String()
        option2 = graphene.String()
        option3 = graphene.String()
        option4 = graphene.String()

    # return response for the enroll_course mutation
    quiz = graphene.Field(QuizNode)
    question = graphene.String()

    # mutation for the current lesson
    def mutate(self, info, quiz_id, question, mark, question_type, **kwargs):
        quiz_title = Quiz.objects.get(id=quiz_id)

        user = info.context.user
        resolve_permission(user)
        if not user.is_author:
            raise GraphQLError("you are not the author")
        # check the user is author for the current course
        if not quiz_title.lesson.module.course.author == user:
            raise GraphQLError("you're not author of this course")

        question = Question(question=question,
                            quiz=quiz_title,
                            mark=mark,
                            question_type=question_type)
        if question_type == "MC":
            option1 = kwargs.get('option1')
            option2 = kwargs.get('option2')
            option3 = kwargs.get('option3')
            option4 = kwargs.get('option4')
            if not option1 or not option2 or not option3 or not option4:
                raise GraphQLError("invalid data")
            question.save()
            ques = Question.objects.get(question=question.question)
            mc_question = QuestionMC.objects.create(question=ques,
                                                    option1=option1,
                                                    option2=option2,
                                                    option3=option3,
                                                    option4=option4)

        elif question_type == "TF":
            option1 = kwargs.get('option1')
            option2 = kwargs.get('option2')
            if not option1 or not option2:
                raise GraphQLError("invalid data")
            question.save()
            ques = Question.objects.get(question=question.question)

            tf_question = QuestionTF.objects.create(question=ques,
                                                    option1=option1.capitalize(),
                                                    option2=option2.capitalize())
        elif question_type == "FB":
            question.save()
            ques = Question.objects.get(question=question.question)
            fb_question = QuestionFB.objects.create(question=ques)

        elif question_type == "ET":
            question.save()
            ques = Question.objects.get(question=question.question)
            et_question = QuestionET.objects.create(question=ques)

        return CreateQuestion(question=question.question,
                              quiz=question.quiz)


class CreateAnswers(graphene.Mutation):
    # arguments for CreateLesson mutation
    class Arguments:
        question_id = graphene.Int(required=True)
        answer = graphene.String(required=True)
        answer_type = graphene.String(required=True)

    # return response for the enroll_course mutation
    user = graphene.Field(UserTypeNode)
    message = graphene.String()

    def mutate(self, info, question_id, answer_type, answer):
        # check the user logged in and author
        user = info.context.user
        resolve_permission(user)
        if not user.is_author:
            raise GraphQLError("you are not the author")
        # filter question by question_id
        ques = Question.objects.get(id=question_id)
        data = CreateAnswer(user=user,
                            question=ques,
                            answer_type=answer_type,
                            answer=answer)
        data.save()
        return CreateAnswers(message='done')


class PostAnswer(graphene.Mutation):
    # arguments for CreateLesson mutation
    class Arguments:
        question_id = graphene.Int(required=True)
        answer = graphene.String(required=True)

    # return response for the enroll_course mutation
    user = graphene.Field(UserTypeNode)
    message = graphene.String()

    # mutation for the current lesson
    def mutate(self, info, question_id, answer):

        question = Question.objects.get(id=question_id)
        user = info.context.user
        resolve_permission(user)

        data = Answer(user=user,
                      question=question,
                      answer_type=question.question_type,
                      answer=answer)
        data.save()
        return PostAnswer(message='done')


class Mutation(object):
    create_quiz = CreateQuiz.Field()
    create_question = CreateQuestion.Field()
    create_answer = CreateAnswers.Field()
    post_answer = PostAnswer.Field()