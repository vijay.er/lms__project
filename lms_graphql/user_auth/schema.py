import graphene
import graphql_jwt
from django.contrib.auth.models import User
from graphene_django.filter import DjangoFilterConnectionField
from graphql import GraphQLError
from graphql_auth import mutations
from .datatypes import UserTypeNode
from lms_graphql.lms.resolvers import resolve_user


class Mutation(object):
    register_user = mutations.Register.Field()
    token_auth = mutations.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()
    revoke_token = mutations.RevokeToken.Field()
    password_change = mutations.PasswordChange.Field()
    update_account = mutations.UpdateAccount.Field()


class Query(graphene.ObjectType):
    all_users = DjangoFilterConnectionField(UserTypeNode)
    user = graphene.Field(UserTypeNode)

    def resolve_user(self, info):
        return resolve_user(info)

    def resolve_all_users(self, info):
        user = info.context.user
        if user.is_superuser:
            return User.objects.all()
        raise GraphQLError("you don't have the permission")
