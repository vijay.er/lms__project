from graphene import Node
from graphene_django import DjangoObjectType
from learningapp.models import CustomUser


class UserTypeNode(DjangoObjectType):
    class Meta:
        model = CustomUser
        interfaces = (Node,)
        filter_fields = '__all__'
