from django.contrib.auth.models import User
from graphql import GraphQLError


def resolve_user(info):
    user = info.context.user
    if user.is_authenticated:
        return user
    raise GraphQLError("you are not logged in")


def resolve_permission(user):
    if user.is_anonymous:
        raise GraphQLError("you are not logged in")
    return user


def resolve_rating_limit(rating):
    if not 0 <= rating <= 5:
        raise GraphQLError("ratings must be 0 to 5")
    return rating


def resolve_completed_status(is_completed):
    if not is_completed == "True":
        raise GraphQLError("invalid data")
    elif not is_completed:
        raise GraphQLError("invalid data")
    elif is_completed == "True":
        return is_completed
