from graphene_django.types import DjangoObjectType
from graphene import Node
from learningapp.models import *
from quiz.models import *


class CategoryNode(DjangoObjectType):
    class Meta:
        model = Category
        interfaces = (Node,)
        filter_fields = '__all__'


class CourseNode(DjangoObjectType):
    class Meta:
        model = Course
        filter_fields = ['title', 'author', 'author_designation', 'language',
                         'rating', 'price', 'what_you_will_learn', 'requirements']
        interfaces = (Node,)


class ModuleNode(DjangoObjectType):
    class Meta:
        model = Module
        filter_fields = ['name', 'duration', 'prerequisite', 'is_active']
        interfaces = (Node,)


class LessonNode(DjangoObjectType):
    class Meta:
        model = Lesson
        filter_fields = ['name']
        interfaces = (Node,)


class RatingNode(DjangoObjectType):
    class Meta:
        model = Rating
        filter_fields = ['course', 'rating', 'user']
        interfaces = (Node,)


class CourseEnrollNode(DjangoObjectType):
    class Meta:
        model = CourseEnroll
        filter_fields = ['username', 'course']
        interfaces = (Node,)


class CompletedLessonsNode(DjangoObjectType):
    class Meta:
        model = CompletedLessons
        interfaces = (Node,)
        filter_fields = '__all__'


class CompletedModulesNode(DjangoObjectType):
    class Meta:
        model = CompletedModules
        interfaces = (Node,)
        filter_fields = '__all__'


class CompletedCoursesNode(DjangoObjectType):
    class Meta:
        model = CompletedCourses
        interfaces = (Node,)
        filter_fields = '__all__'
