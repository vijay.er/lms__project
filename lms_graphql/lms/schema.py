from graphene_django.filter import DjangoFilterConnectionField
from lms_graphql.lms.mutation import *
from lms_graphql.lms.resolvers import resolve_user


class Query(graphene.ObjectType):
    all_courses = DjangoFilterConnectionField(CourseNode)
    course = graphene.List(CourseNode)

    def resolve_all_courses(self, info):
        resolve_user(info)
        return Course.objects.all()

    def resolve_course(self, info):
        resolve_user(info)
        return Course.objects.all()

    all_modules = DjangoFilterConnectionField(ModuleNode)
    module = graphene.List(ModuleNode)

    def resolve_all_modules(self, info):
        resolve_user(info)
        return Module.objects.all()

    def resolve_modules(self, info):
        resolve_user(info)
        return Module.objects.all()

    all_lesson = DjangoFilterConnectionField(LessonNode)
    lesson = graphene.List(LessonNode)

    def resolve_all_lesson(self, info):
        resolve_user(info)
        return Lesson.objects.all()

    def resolve_lesson(self, info):
        resolve_user(info)
        return Lesson.objects.all()

    all_rating = DjangoFilterConnectionField(RatingNode)
    rating = graphene.List(RatingNode)

    def resolve_all_rating(self, info):
        resolve_user(info)
        return Rating.objects.all()

    def resolve_rating(self, info):
        resolve_user(info)
        return Rating.objects.all()

    all_course_enrollment = DjangoFilterConnectionField(CourseEnrollNode)
    course_enrollment = graphene.List(CourseEnrollNode)

    def resolve_all_course_enrollment(self, info):
        resolve_user(info)
        return CourseEnroll.objects.all()

    def resolve_course_enrollment(self, info):
        resolve_user(info)
        return CourseEnroll.objects.all()

    all_complete_lesson = DjangoFilterConnectionField(CompletedLessonsNode)
    comp_lesson = graphene.List(CompletedLessonsNode)

    def resolve_all_complete_lesson(self, info):
        resolve_user(info)
        return CompletedLessons.objects.all()

    def resolve_comp_lesson(self, info):
        resolve_user(info)
        return CompletedLessons.objects.all()

    all_complete_module = DjangoFilterConnectionField(CompletedModulesNode)
    comp_module = graphene.List(CompletedModulesNode)

    def resolve_all_complete_module(self, info):
        resolve_user(info)
        return CompletedModules.objects.all()

    def resolve_comp_module(self, info):
        resolve_user(info)
        return CompletedModules.objects.all()

    all_complete_course = DjangoFilterConnectionField(CompletedCoursesNode)
    comp_course = graphene.List(CompletedCoursesNode)

    def resolve_all_complete_course(self, info):
        resolve_user(info)
        return CompletedCourses.objects.all()

    def resolve_comp_course(self, info):
        resolve_user(info)
        return CompletedCourses.objects.all()
