import graphene
from graphene_file_upload.scalars import Upload
from graphql import GraphQLError
from lms_graphql.lms.datatypes import *
from lms_graphql.lms.resolvers import resolve_permission, resolve_rating_limit, resolve_completed_status
from lms_graphql.user_auth.datatypes import UserTypeNode


class CreateCourse(graphene.Mutation):
    # arguments for CreateCourse mutation
    class Arguments:
        title = graphene.String(required=True)
        featured_image = Upload()
        author_designation = graphene.String(required=True)
        stats = graphene.String(required=True)
        description = graphene.String(required=True)
        language = graphene.String(required=True)
        price = graphene.Int(required=True)
        what_you_will_learn = graphene.String(required=True)
        requirements = graphene.String(required=True)

    # return response for the CreateCourse mutation
    author = graphene.String()
    title = graphene.String()

    # mutation for the current course
    def mutate(self, info, title, author_designation,
               stats, description, language, price, what_you_will_learn, requirements):
        img = info.context.FILES['featured_image']
        user = info.context.user
        resolve_permission(user)

        if not user.is_author:
            raise GraphQLError("you are not the author")

        try:
            Course.objects.get(title=title)
        except:
            course = Course(title=title,
                            featured_image=img,
                            author=user,
                            author_designation=author_designation,
                            stats=stats,
                            description=description,
                            language=language,
                            price=price,
                            what_you_will_learn=what_you_will_learn,
                            requirements=requirements)
            course.save()
            return CreateCourse(author=course.author,
                                title=course.title)
        else:
            raise GraphQLError("course already exists")


class CreateModule(graphene.Mutation):
    # arguments for CreateModule mutation
    class Arguments:
        name = graphene.String(required=True)
        prerequisite = graphene.String(required=True)
        course_id = graphene.Int(required=True)

    # return response for the CreateModule mutation
    course = graphene.Field(CourseNode)
    name = graphene.String()

    # mutation for the current module
    def mutate(self, info, name, prerequisite, course_id):
        user = info.context.user
        resolve_permission(user)
        if not user.is_author:
            raise GraphQLError("you are not the author")
        try:
            Module.objects.get(name=name)
        except:
            course_name = Course.objects.get(id=course_id)
            if not course_name.author == user:
                raise GraphQLError("you don't have permission to create module")
            module = Module(name=name,
                            prerequisite=prerequisite,
                            course=course_name)
            module.save()
            return CreateModule(course=module.course,
                                name=module.name)

        else:
            raise GraphQLError("module already exists")


class CreateLesson(graphene.Mutation):
    # arguments for CreateLesson mutation
    class Arguments:
        name = graphene.String(required=True)
        featured_image = Upload()
        description = graphene.String(required=True)
        video = Upload()
        file = Upload()
        transcript = graphene.String(required=True)
        prerequisite = graphene.String(required=True)
        module_id = graphene.Int(required=True)

    # return response for the CreateLesson mutation
    module = graphene.Field(ModuleNode)
    lesson = graphene.String()

    # mutation for the current lesson
    def mutate(self, info, name, description, transcript, prerequisite, module_id):
        module_name = Module.objects.get(id=module_id)
        featured_img = info.context.FILES['featured_image']
        lesson_video = info.context.FILES['lesson_video']
        lesson_file = info.context.FILES['lesson_file']
        user = info.context.user
        resolve_permission(user)
        if not user.is_author:
            raise GraphQLError("you are not the author")
        try:
            Lesson.objects.get(name=name)
        except:
            if not module_name.course.author == user:
                raise GraphQLError("you don't have permission to create lesson")
            lesson = Lesson(name=name,
                            featured_image=featured_img,
                            description=description,
                            video=lesson_video,
                            file=lesson_file,
                            transcript=transcript,
                            prerequisite=prerequisite,
                            module=module_name)
            lesson.save()
            return CreateLesson(module=lesson.module,
                                lesson=lesson.name)
        else:
            raise GraphQLError("lesson already exists")


class CreateRating(graphene.Mutation):
    # arguments for CreateRating mutation
    class Arguments:
        course_id = graphene.Int(required=True)
        rating = graphene.Int(required=True)

    # return response for the CreateRating mutation
    user = graphene.Field(UserTypeNode)
    course = graphene.Field(CourseNode)

    # mutation for the rating the course
    def mutate(self, info, course_id, rating):
        user = info.context.user

        # check the user logged in or not
        resolve_permission(user)
        resolve_rating_limit(rating)

        # get course by course_id
        course_name = Course.objects.get(id=course_id)
        if course_name.author == user:
            raise GraphQLError("you can't rate your own course")

        if not course_name.ratings.filter(user=user):
            rating = Rating(user=user,
                            rating=rating,
                            course=course_name)
            rating.save()
            return CreateRating(user=rating.user,
                                course=rating.course)
        raise GraphQLError("already rated the course")


class EnrollCourse(graphene.Mutation):
    # arguments for enroll_course mutation
    class Arguments:
        course_id = graphene.Int(required=True)

    # return response for the enroll_course mutation
    username = graphene.Field(UserTypeNode)
    course = graphene.Field(CourseNode)

    # mutation for the enroll course
    def mutate(self, info, course_id):
        course_name = Course.objects.get(id=course_id)
        user = info.context.user
        resolve_permission(user)

        if course_name.author == user:
            raise GraphQLError("you can't enroll your own course")
        if not course_name.course_enroll.filter(username=user):
            enroll = CourseEnroll(username=user,
                                  course=course_name)
            enroll.save()
            return CourseEnroll(username=enroll.username,
                                course=enroll.course,
                                )
        raise GraphQLError(message="already enrolled")


class UpdateCourse(graphene.Mutation):
    # arguments for UpdateCourse mutation
    class Arguments:
        title = graphene.String(required=True)
        featured_image = Upload()
        author_designation = graphene.String(required=True)
        stats = graphene.String(required=True)
        description = graphene.String(required=True)
        language = graphene.String(required=True)
        price = graphene.Int(required=True)
        what_you_will_learn = graphene.String(required=True)
        requirements = graphene.String(required=True)
        course_id = graphene.Int(required=True)

    # return response for the UpdateCourse mutation
    author = graphene.Field(UserTypeNode)
    title = graphene.String()

    # mutation for the current course
    def mutate(self, info, title, author_designation,
               stats, description, language, price, course_id, what_you_will_learn, requirements):
        img = info.context.FILES['featured_image']
        user = info.context.user
        # check the user for login
        resolve_permission(user)

        # check the course available or not
        course = Course.objects.filter(id=course_id)
        if not course:
            raise GraphQLError("invalid details")

        # check that course with author name (permission to update)
        if not Course.objects.get(id=course_id).author == user:
            raise GraphQLError("you don't have permission to update the course")

        # update the course filtered by course_id arg
        course.update(title=title,
                      featured_image=img,
                      author=user,
                      author_designation=author_designation,
                      stats=stats,
                      description=description,
                      language=language,
                      price=price,
                      what_you_will_learn=what_you_will_learn,
                      requirements=requirements)
        return UpdateCourse(author=user,
                            title=title)


class CompletedLesson(graphene.Mutation):
    # arguments for CreateLesson mutation
    class Arguments:
        lesson_id = graphene.Int(required=True)
        is_completed = graphene.String(required=True)

    # return response for the CompletedLesson mutation
    user = graphene.Field(UserTypeNode)
    lesson = graphene.Field(LessonNode)

    # mutation for the current lesson
    def mutate(self, info, lesson_id, is_completed):

        # check the user for login
        user = info.context.user
        resolve_permission(user)
        resolve_completed_status(is_completed)

        lesson_name = Lesson.objects.get(id=lesson_id)
        # check the author name for the same course
        if lesson_name.module.course.author == user:
            raise GraphQLError("you can't complete your own course")

        # check user already completed the same lesson or not
        if not lesson_name.completed_lesson.filter(user=user):
            data = CompletedLessons(user=user,
                                    lesson=lesson_name,
                                    is_active=True)
            data.save()
            return CompletedLessons(user=data.user,
                                    lesson=data.lesson)

        raise GraphQLError("already completed")


class CompletedModule(graphene.Mutation):
    # arguments for CompletedModule mutation
    class Arguments:
        module_id = graphene.Int(required=True)
        is_completed = graphene.String(required=True)

    # return response for the CompletedModule mutation
    user = graphene.Field(UserTypeNode)
    module = graphene.Field(ModuleNode)

    # mutation for the CompletedModule
    def mutate(self, info, module_id, is_completed):
        user = info.context.user

        # check the user is authenticated or not
        resolve_permission(user)
        resolve_completed_status(is_completed)

        module_name = Module.objects.get(id=module_id)

        # check the author name for the current module
        if module_name.course.author == user:
            raise GraphQLError("you can't complete your own module")

        # check the user already completed the same module or not
        if not module_name.completed_module.filter(user=user):
            data = CompletedModules(user=user,
                                    module=module_name,
                                    is_active=True)
            data.save()
            return CompletedModule(user=data.user,
                                   module=data.module)

        raise GraphQLError("already completed")


class CompletedCourse(graphene.Mutation):
    # arguments for CompletedCourse mutation
    class Arguments:
        course_id = graphene.Int(required=True)
        is_completed = graphene.String(required=True)

    # return response for the CompletedCourse mutation
    user = graphene.Field(UserTypeNode)
    course = graphene.Field(CourseNode)

    # mutation for the CompletedCourse
    def mutate(self, info, course_id, is_completed):

        # check user permission
        user = info.context.user
        resolve_permission(user)
        resolve_completed_status(is_completed)

        course_name = Course.objects.get(id=course_id)

        if course_name.author == user:
            raise GraphQLError("you can't complete your own course")

        # check the user already completed the same course or not
        if not course_name.completed_course.filter(user=user):
            data = CompletedCourses(user=user,
                                    course=course_name,
                                    is_active=True)
            data.save()
            return CompletedCourse(user=data.user,
                                   course=data.course)

        raise GraphQLError("already completed")


class Mutation(object):
    create_course = CreateCourse.Field()
    create_module = CreateModule.Field()
    create_lesson = CreateLesson.Field()

    course_enroll = EnrollCourse.Field()
    create_rating = CreateRating.Field()

    update_course = UpdateCourse.Field()

    completed_lesson = CompletedLesson.Field()
    completed_module = CompletedModule.Field()
    completed_course = CompletedCourse.Field()